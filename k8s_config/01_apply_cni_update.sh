#!/bin/bash
kubectl apply -f https://raw.githubusercontent.com/aws/amazon-vpc-cni-k8s/master/config/v1.3/aws-k8s-cni.yaml

echo "Remember to set AWS_VPC_K8S_CNI_EXTERNALSNAT : true"
echo "https://docs.aws.amazon.com/eks/latest/userguide/external-snat.html"
