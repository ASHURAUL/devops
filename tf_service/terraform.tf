terraform {
  backend "s3" {
    key     = "services_tfstate"
    region  = "eu-west-1"
    encrypt = true
  }

  required_version = "~> 0.11.10"
}
