locals {
  env = "${terraform.workspace == "default" ? "dev" : terraform.workspace}"

  env_sshpubkey_file = {
    "dev"  = "${var.dev_sshpubkey_file}"
    "prod" = "${var.prod_sshpubkey_file}"
  }

  sshpubkey_file = "${local.env_sshpubkey_file["foo"]}"

  common_tags = {
    "Terraform"   = "true"
    "Environment" = "${local.env}"
  }
}
