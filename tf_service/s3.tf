resource "random_pet" "my-s3" {
  length    = "2"
  prefix    = "my-s3"
  separator = "-"
}

module "my-s3" {
  source      = "./modules/s3"
  bucket_name = "${random_pet.my-s3.id}"
}
